<?php

namespace Drupal\node_field_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Displays node field as a block
 *
 * @Block(
 *   id = "node_field_block",
 *   admin_label = @Translation("Node Field Block")
 * )
 */
class NodeFieldBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The Entity Type Manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface;
   */
  protected $entityTypeManager;

  /**
   * Entity Field Manager
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The Entity Display Repository
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepository
   */
  protected $entityDisplayRepository;

  /**
   * Dependency injection through the constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The Entity Field Manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The Entity Display Repository.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $types = array_keys(node_type_get_names());
    return array(
      'node_field_block_settings' => [
        'node_type' => reset($types),
        'settings'=>[
          'view_mode' => 'default',
          'field' => '',
        ]
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $types = node_type_get_names();
    $config = $this->configuration['node_field_block_settings'];

    if(empty($config['node_type']) || (!empty($config['node_type']) && isset($form_state->getUserInput()['settings']['node_field_block_settings']['node_type']) && $config['node_type'] != $form_state->getUserInput()['settings']['node_field_block_settings']['node_type'])){
      $config['node_type'] = $form_state->getUserInput()['settings']['node_field_block_settings']['node_type'];
    }


    $form['node_field_block_settings'] = array(
      '#title' => $this->t('Select Content Type which you want to display in block'),
      '#type' => 'fieldset',
      '#prefix' => '<div id="node-field-block-wrapper">',
      '#suffix' => '</div>',
    );

    $form['node_field_block_settings']['node_type'] = array(
      '#title' => $this->t('Content type'),
      '#description' => $this->t('Content type which contains field'),
      '#type' => 'select',
      '#options' => $types,
      '#default_value' => $config['node_type'],
      '#ajax' => array(
        'callback' => array(get_class($this), 'updateFieldList'),
        'wrapper' => 'node-field-block-settings-wrapper',
      ),
    );

    $form['node_field_block_settings']['settings'] = array(
      '#title' => $this->t('Select view mode and field you want to display in block'),
      '#type' => 'fieldset',
      '#prefix' => '<div id="node-field-block-settings-wrapper">',
      '#suffix' => '</div>',
    );

    $form['node_field_block_settings']['settings']['view_mode'] = array(
      '#title' => $this->t('View mode'),
      '#description' => $this->t('View mode in which field has field formatter selected'),
      '#type' => 'select',
      '#multiple' => FALSE,
      '#options' => $this->getViewModes($config['node_type']),
      '#default_value' => $config['settings']['view_mode'],
    );

    $form['node_field_block_settings']['settings']['field'] = array(
      '#title' => $this->t('Field'),
      '#description' => $this->t('Field you want to display in block'),
      '#type' => 'select',
      '#multiple' => FALSE,
      '#options' =>  $this->getFieldList($config['node_type']),
      '#default_value' => $config['settings']['field'],
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['node_field_block_settings'] = $form_state->getValue('node_field_block_settings');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configuration['node_field_block_settings'];
    $build = array();
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      if ($config['node_type'] == $node->getType()) {
        if ($field = $node->get($config['settings']['field'])) {
          $build = $node->{$config['settings']['field']}->view($config['settings']['view_mode']);
          return array(
            '#markup' => render($build),
          );
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    } else {
      return parent::getCacheTags();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }

  /**
   * Get field list
   *
   * @param string $node_type
   * @return array
   */
  protected function getFieldList($node_type) {
    $fields = [];
    if (!empty($node_type)) {
      $fields = $this->entityFieldManager->getFieldDefinitions('node', $node_type);
      foreach ($fields as $id => $field) {
        if($field instanceof FieldConfig) {
          $fields[$id] = $field->label();
        }else{
          unset($fields[$id]);
        }
      }
    }
    return $fields;
  }

  /**
   * Get view modes
   *
   * @param string $node_type id of node type
   * @return array view modes by bundle
   */
  protected function getViewModes($node_type) {
    return $this->entityDisplayRepository->getViewModeOptionsByBundle('node', $node_type);
  }

  /**
   * Handles switching the node type selector.
   */
  public static function updateFieldList(&$form, FormStateInterface &$form_state, Request $request) {
    return $form['settings']['node_field_block_settings']['settings'];
  }
}
